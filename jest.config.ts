const config = {
  moduleFileExtensions: [
    'ts',
    'js',
  ],
  transform: {
    '^.+\\.(ts|tsx)$': ['ts-jest', { tsconfig: 'tsconfig.json' }],
  },
  testRegex: 'test/.*\.test\.ts$',
  testEnvironment: 'node',
  setupFiles: [ '<rootDir>/jest.setup.js'],
};

export default config;