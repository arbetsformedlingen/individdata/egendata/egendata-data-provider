import nodeFetch from 'node-fetch';
import { defaultAuthFetchFactory, OidcAuthFetchFactory } from '../../src/util/OidcAuthFetchFactory';


describe('OidcAuthFetchFactory getAuthFetch', () => {
  it('should return nodeFetch in TEST environment ', async () => {
    const fetchFactory = new OidcAuthFetchFactory();   
    
    const authFetch = await fetchFactory.getAuthFetch('test');

    expect(authFetch).toEqual(nodeFetch);
  });

  it('should fetch an access token and build an authenticated fetch if authorized', async () => {
    const fetchFactory = new OidcAuthFetchFactory();

    const accessTokenfetcherMock = jest.fn();
    accessTokenfetcherMock.mockResolvedValueOnce({ accessToken: 'token', dpopKey: 'key' });

    const authenticatedFetchBuilderMock = jest.fn();
    authenticatedFetchBuilderMock.mockResolvedValueOnce(nodeFetch);
    
    await fetchFactory.getAuthFetch('', 'id', 'secret', accessTokenfetcherMock, authenticatedFetchBuilderMock);

    expect(accessTokenfetcherMock).toBeCalledWith('id', 'secret');
    expect(authenticatedFetchBuilderMock).toBeCalledWith(nodeFetch, 'token', { dpopKey: 'key' });
  });

  it('should throw an exception if not authorized', async () => {
    const fetchFactory = new OidcAuthFetchFactory();

    const error = new Error('unauthorized');

    const accessTokenfetcherMock = jest.fn();
    accessTokenfetcherMock.mockRejectedValueOnce(error);

    const authenticatedFetchBuilderMock = jest.fn();
    
    const authFetchPromise = fetchFactory.getAuthFetch('', 'bad', 'user', accessTokenfetcherMock, authenticatedFetchBuilderMock);

    await expect(authFetchPromise).rejects.toThrow(error);
    expect(authenticatedFetchBuilderMock).toBeCalledTimes(0);
  });

});

describe('defaultAuthFetchFactory getAuthFetch', () => {
  it('should return nodeFetch in TEST environment', async () => {
    
    const authFetch = await defaultAuthFetchFactory.getAuthFetch();
    expect(authFetch).toBe(nodeFetch);
  });

});

describe('defaultAuthFetchFactory fetchAccessTokenKey', () => {
  it('should return access token and dpop key', async () => {

    const fetchFactory = new OidcAuthFetchFactory();
    const keyPairGeneratorMock = jest.fn();
    keyPairGeneratorMock.mockResolvedValueOnce('dpopkey');

    const headerCreatorMock = jest.fn();
    headerCreatorMock.mockResolvedValueOnce('header');

    const jsonMock = jest.fn();
    jsonMock.mockResolvedValueOnce({ access_token: 'accessToken' });

    const fetchFunctionMock = jest.fn();
    fetchFunctionMock.mockResolvedValueOnce({ ok: true, json: jsonMock });

    const tokenKey = await fetchFactory.fetchAccessTokenKey('id', 'secret', 'idp', keyPairGeneratorMock, headerCreatorMock, fetchFunctionMock as any);
    expect(tokenKey).toStrictEqual({ accessToken: 'accessToken', dpopKey: 'dpopkey' });
  });

  it('should throw error if token request unsuccessful', async () => {

    const fetchFactory = new OidcAuthFetchFactory();
    const keyPairGeneratorMock = jest.fn();
    keyPairGeneratorMock.mockResolvedValueOnce('dpopkey');

    const headerCreatorMock = jest.fn();
    headerCreatorMock.mockResolvedValueOnce('header');

    const error = new Error('unauthorized');
    const description = 'wrong password';
    const jsonMock = jest.fn();
    jsonMock.mockResolvedValueOnce({ error, error_description: description });

    const fetchFunctionMock = jest.fn();
    fetchFunctionMock.mockResolvedValueOnce({ ok: false, json: jsonMock });

    const fetchAccessTokenKeyPromise = fetchFactory.fetchAccessTokenKey('id', 'secret', 'idp', keyPairGeneratorMock, headerCreatorMock, fetchFunctionMock as any);

    const expectedError = new Error('Could not fetch access token: Error: unauthorized, wrong password');

    await expect(fetchAccessTokenKeyPromise).rejects.toThrow(expectedError);
    expect(keyPairGeneratorMock).toBeCalledTimes(1);
    expect(headerCreatorMock).toBeCalledTimes(1);
    expect(fetchFunctionMock).toBeCalledTimes(1);
  });

});
