import { containerResources, deleteResource, egendataInitialization, egendataPrefix, fetchContainerData, handleResources, inboxHandler, inboxHandlerImpl, lockedContainerResourceHandler, parseDataRequestData, parseInboxResource, saveLinkToInbox, writeVerifiableCredential, scanContainer, scanInboxWithNewToken, storageUrlFromProfile, StoreFunction, subscribeToInbox, unsubscribe, __removeDataRequestLink, __getBaseUrl, __getObjectsForPredicate, __getSubscriptionUrl, __initializeEgendata, __put, __fetchCredentialSubject } from '../../src/util/solid';
import { webid } from '../../src/config';
import { AuthFetchFunction } from '../../src/util/AuthFetchFactory';

jest.mock('../../src/util/vc', () => ({
  __esModule: true,
  issueVerifiableCredential: jest.fn(),
}));

//jest.mock('pino');

describe('parseInboxResource', () => {
  it('should extract the request URL ', () => {
  // given
    const inboxResource = `@prefix egendata: <${egendataPrefix}> .
    <> egendata:OutboundDataRequest <http://localhost:3001/testRequest>.`;

    expect(parseInboxResource(inboxResource)).toMatchObject({ outboundDataRequest: 'http://localhost:3001/testRequest' });
  });

  it('should throw an Error if no egendata:OutboundDataRequest found in the resource', () => {
    // given
    const inboxResource = `@prefix egendata: <${egendataPrefix}> .
    <> egendata:DummyRequest <http://localhost:3001/dummyRequest>.`;

    expect(() => parseInboxResource(inboxResource)).toThrow();
  });
});

describe('storageUrlFromProfile', () => {
  it('should return storage url', () => {
    // given correct data
    const profileData = `@prefix foaf: <http://xmlns.com/foaf/0.1/>.
    @prefix solid: <http://www.w3.org/ns/solid/terms#>.
    @prefix space: <http://www.w3.org/ns/pim/space#>.
    @prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#>.
    
    <>
        a foaf:PersonalProfileDocument;
        foaf:maker <http://localhost:3002/88d01bca-99ea-4a1b-8544-d6a1ecc7b709/profile/card#me>;
        foaf:primaryTopic <http://localhost:3002/88d01bca-99ea-4a1b-8544-d6a1ecc7b709/profile/card#me>.
    
    <http://localhost:3002/88d01bca-99ea-4a1b-8544-d6a1ecc7b709/profile/card#me>
        a foaf:Person;
        
        solid:oidcIssuer <http://localhost:3002/>;
        space:storage <http://localhost:3001/6c906207-41c5-4449-989f-b74e3e5f6eb3/>;
        rdfs:seeAlso <./private>.`;

    const expectedUrl = 'http://localhost:3001/6c906207-41c5-4449-989f-b74e3e5f6eb3/';

    expect(storageUrlFromProfile(profileData)).toEqual(expectedUrl);
  });
});

describe('parseDataRequestData', () => {
  it('should return extracted data', () => {
    // given correct data
    const inboxResource = `@prefix egendata: <${egendataPrefix}> .
    <> a egendata:OutboundDataRequest ;
      egendata:documentType "https://egendata.se/schema/core/v1#JobSeekerRegistrationStatus" ;
      egendata:dataSubjectIdentifier "199010102383" ;
      egendata:dataLocation "http://localhost:3001/testLocation" ;
      egendata:notificationInbox "http://localhost:3001/test/inbox/" ;
      <http://purl.org/dc/terms/created> "2022-12-29T13:15:06.198Z"^^<http://www.w3.org/2001/XMLSchema#dateTime> .`;

    const expectedRequest = {
      documentType: 'https://egendata.se/schema/core/v1#JobSeekerRegistrationStatus', 
      dataSubjectIdentifier: '199010102383',
      dataLocation: 'http://localhost:3001/testLocation',
      notificationInbox: 'http://localhost:3001/test/inbox/',
    };

    expect(parseDataRequestData(inboxResource)).toMatchObject(expectedRequest);
  });
});

describe('saveVCToDataLocation', () => {
  it('should store the vc', async () => {

    const authFetchMock = jest.fn();
    // given correct data
    const doc = {};

    // const document = Buffer.from(JSON.stringify(doc), 'utf-8').toString('base64');
    const expected = `@prefix egendata: <${egendataPrefix}> .
<> a egendata:InboundDataResponse ;
  egendata:requestId "id" ;
  egendata:providerWebId "${webid}" ;
  egendata:document "e30=" .
`;

    await writeVerifiableCredential('id', 'dataLocation', doc, authFetchMock as AuthFetchFunction);
    
    const args = authFetchMock.mock.lastCall;
    expect(args).toHaveLength(2);
    expect(args[0]).toBe('dataLocation');
    expect(args[1].method).toBe('PUT');
    expect(args[1].body).toBe(expected);
  });

});

describe('saveLinkToInbox', () => {
  it('should store the link', async () => {
    const authFetchMock = jest.fn();

    //given url and link
    const url = 'http://inbox.url/';
    const link = 'http://link.url/';

    const id = 'test';
  
    const expectedUrl = `${url}response-link-${id}`;
    const expected = `
@prefix egendata: <${egendataPrefix}> .
<> a egendata:InboundDataResponseLink ;
   egendata:InboundDataResponse <${link}> .
`;

    await saveLinkToInbox(url, link, authFetchMock, () => id);

    const args = authFetchMock.mock.lastCall;
    expect(args).toHaveLength(2);
    expect(args[0]).toBe(expectedUrl);
    expect(args[1].method).toBe('PUT');
    expect(args[1].body).toBe(expected);
  });
});

describe('deleteResource', () => {
  it('should delete the link', async () => {
    const authFetchMock = jest.fn();

    //given url
    const url = 'http://link.url/';

    await deleteResource(url, authFetchMock);

    const args = authFetchMock.mock.lastCall;
    expect(args).toHaveLength(2);
    expect(args[0]).toBe(url);
    expect(args[1].method).toBe('DELETE');
  });
});

describe('unsubscribe', () => {
  it('should delete the unsubscription resource', async () => {
    const authFetchMock = jest.fn();

    //given
    const unsubscriptionUrl = 'http://pod.storage/unsubsribe';

    await unsubscribe(unsubscriptionUrl, authFetchMock);

    const args = authFetchMock.mock.lastCall;
    expect(args).toHaveLength(2);
    expect(args[0]).toBe(unsubscriptionUrl);
    expect(args[1].method).toBe('DELETE');
  });
});

describe('subscribeToInbox', () => {
  
  it('should subscribe to webhook notifications for egendata/inbox', async () => {
    // given
    const storageUrl = 'https://storage:1234/';
    const subscriptionUrl = 'https://example.com/some/path';
    const subscriptionEndpoints = `
    <http://example.com/.well-known/solid> a <http://www.w3.org/ns/pim/space#Storage>;
        <http://www.w3.org/ns/solid/notifications#subscription> <http://example.com/.notifications/WebSocketChannel2023/>, <https://example.com/some/path>.
    <http://example.com/.notifications/WebSocketChannel2023/> <http://www.w3.org/ns/solid/notifications#channelType> <http://www.w3.org/ns/solid/notifications#WebSocketChannel2023>;
        <http://www.w3.org/ns/solid/notifications#feature> <http://www.w3.org/ns/solid/notifications#accept>, <http://www.w3.org/ns/solid/notifications#endAt>, <http://www.w3.org/ns/solid/notifications#rate>, <http://www.w3.org/ns/solid/notifications#startAt>, <http://www.w3.org/ns/solid/notifications#state>.
    <https://example.com/some/path> <http://www.w3.org/ns/solid/notifications#channelType> <http://www.w3.org/ns/solid/notifications#WebHookChannel2023>;
        <http://www.w3.org/ns/solid/notifications#feature> <http://www.w3.org/ns/solid/notifications#accept>, <http://www.w3.org/ns/solid/notifications#endAt>, <http://www.w3.org/ns/solid/notifications#rate>, <http://www.w3.org/ns/solid/notifications#startAt>, <http://www.w3.org/ns/solid/notifications#state>.
    `;
  
    const authFetchMock = jest.fn();
    authFetchMock.mockResolvedValueOnce({ text: jest.fn(() => (subscriptionEndpoints)) });
    authFetchMock.mockResolvedValueOnce({ json: jest.fn(() => ({ unsubscribe_endpoint: 'http://example.com/unsubscribe' })), ok: true });

    await subscribeToInbox(storageUrl, authFetchMock);

    expect(authFetchMock).toBeCalledTimes(2);

    const args = authFetchMock.mock.lastCall;
    expect(args[0]).toBe(subscriptionUrl);
    expect(args[1].method).toBe('POST');

  });

  it('should throw an exception if no webhook notifications are supported', async () => {
    // given
    const storageUrl = 'https://storage:1234/';
    const subscriptionEndpoints = `
    <http://example.com/.well-known/solid> a <http://www.w3.org/ns/pim/space#Storage>;
        <http://www.w3.org/ns/solid/notifications#subscription> <http://example.com/.notifications/WebSocketChannel2023/>.
    <http://example.com/.notifications/WebSocketChannel2023/> <http://www.w3.org/ns/solid/notifications#channelType> <http://www.w3.org/ns/solid/notifications#WebSocketChannel2023>;
        <http://www.w3.org/ns/solid/notifications#feature> <http://www.w3.org/ns/solid/notifications#accept>, <http://www.w3.org/ns/solid/notifications#endAt>, <http://www.w3.org/ns/solid/notifications#rate>, <http://www.w3.org/ns/solid/notifications#startAt>, <http://www.w3.org/ns/solid/notifications#state>.
    `;
  
    const authFetchMock = jest.fn();
    authFetchMock.mockResolvedValueOnce({ text: jest.fn(() => (subscriptionEndpoints)) });

    
    await expect(subscribeToInbox(storageUrl, authFetchMock)).rejects.toThrowError('No subscription URL found');
  });

  it('should throw an exception if POST to subscription endpoint fails', async () => {
    // given
    const storageUrl = 'https://storage:1234/';
    const subscriptionEndpoints = `
    <http://example.com/.well-known/solid> a <http://www.w3.org/ns/pim/space#Storage>;
        <http://www.w3.org/ns/solid/notifications#subscription> <http://example.com/.notifications/WebSocketChannel2023/>, <https://example.com/some/path>.
    <http://example.com/.notifications/WebSocketChannel2023/> <http://www.w3.org/ns/solid/notifications#channelType> <http://www.w3.org/ns/solid/notifications#WebSocketChannel2023>;
        <http://www.w3.org/ns/solid/notifications#feature> <http://www.w3.org/ns/solid/notifications#accept>, <http://www.w3.org/ns/solid/notifications#endAt>, <http://www.w3.org/ns/solid/notifications#rate>, <http://www.w3.org/ns/solid/notifications#startAt>, <http://www.w3.org/ns/solid/notifications#state>.
    <https://example.com/some/path> <http://www.w3.org/ns/solid/notifications#channelType> <http://www.w3.org/ns/solid/notifications#WebHookChannel2023>;
        <http://www.w3.org/ns/solid/notifications#feature> <http://www.w3.org/ns/solid/notifications#accept>, <http://www.w3.org/ns/solid/notifications#endAt>, <http://www.w3.org/ns/solid/notifications#rate>, <http://www.w3.org/ns/solid/notifications#startAt>, <http://www.w3.org/ns/solid/notifications#state>.
    `;

    const authFetchMock = jest.fn();
    authFetchMock.mockResolvedValueOnce({ text: jest.fn(() => (subscriptionEndpoints)) });
    authFetchMock.mockResolvedValueOnce({ json: jest.fn(() => ({ unsubscribe_endpoint: 'http://example.com/unsubscribe' })), ok: false });

    
    await expect(subscribeToInbox(storageUrl, authFetchMock)).rejects.toThrowError('Subscription failed');
  });


});

describe('fetchContainerData', () => {
  it('should fetch text (turtle) from the server', async () => {

    const resourceUrl = 'http://test.domain/resource/';
    const resourceData = `
    <> a <http://www.w3.org/ns/ldp#Container>.
    `;

    const authFetchMock = jest.fn();
    authFetchMock.mockResolvedValueOnce({ text: jest.fn(() => (resourceData)) });


    const result = await fetchContainerData(resourceUrl, authFetchMock);

    const args = authFetchMock.mock.lastCall;
    expect(args).toHaveLength(1);
    expect(args[0]).toBe(resourceUrl);
    expect(result).toEqual(resourceData);
  });
});

describe('containerResources', () => {
  it('should give all container resources as an array', async () => {

    const containerUrl = 'http://test.domain/resource/';
    const containerData = `
@prefix ldp: <http://www.w3.org/ns/ldp#>.

<> a ldp:Container, ldp:BasicContainer, ldp:Resource;
    ldp:contains <a>, <c>, <b>.
`;
    const result = containerResources(containerUrl, containerData);

    expect(result).toEqual(['http://test.domain/resource/a', 'http://test.domain/resource/c', 'http://test.domain/resource/b']);
  });
});

//handleResources
describe('handleResources', () => {
  it('should handle all resources', async () => {
    const handler = jest.fn();
    handler.mockResolvedValue({ status: 0 });

    //given
    const resources = [
      'reource1',
      'reource2',
      'reource3',
    ];
    const key = 'key';

    await handleResources(resources, key, handler);

    expect(handler).toBeCalledTimes(3);
  });
  
  it('should stop handling of resources as soon as error status 500 accurs', async () => {
    const handler = jest.fn();
    handler
      .mockResolvedValueOnce({ status: 'OK', result: 'result text' })
      .mockResolvedValueOnce({ status: 500, result: 'error' })
      .mockResolvedValueOnce({ status: 'OK', result: 'result text' });

    //given
    const resources = [
      'reource1',
      'reource2',
      'reource3',
    ];
    const key = 'key';

    await handleResources(resources, key, handler);

    expect(handler).toBeCalledTimes(2);
  });
});

describe('scanContainer', () => {
  it('should give all container resources as an array', async () => {

    const handler = jest.fn();
    handler.mockResolvedValue({ status: 0, result: 'result text' });

    const containerUrl = 'http://test.domain/user/inbox/';
    const containerData = `
@prefix ldp: <http://www.w3.org/ns/ldp#>.

<> a ldp:Container, ldp:BasicContainer, ldp:Resource;
    ldp:contains <a>, <c>, <b>.
`;
    const key = 'key';
    await scanContainer(containerUrl, containerData, key, handler);

    expect(handler).toBeCalledTimes(3);
  });
});

describe('egendataInitialization', () => {
  it('should initialize egendata resources if not already done', async () => {

    const authFetchMock = jest.fn();
    authFetchMock.mockResolvedValue({ status: 404, result: 'result text' });

    //given
    const webId = 'http://idp/testuser/profile#me';
    const storagePodUrl = 'http://pod/testuser/';

    await egendataInitialization(webId, storagePodUrl, authFetchMock);

    expect(authFetchMock).toBeCalledTimes(4);
  });

  it('should skip initialization if already done', async () => {

    const authFetchMock = jest.fn();
    authFetchMock.mockResolvedValue({ status: 200, result: 'result text' });

    //given
    const webId = 'http://idp/testuser/profile#me';
    const storagePodUrl = 'http://pod/testuser/';

    await egendataInitialization(webId, storagePodUrl, authFetchMock);

    expect(authFetchMock).toBeCalledTimes(1);
  });

});

// #############################################
// test not exported functions
// #############################################
describe('getSubscriptionUrl', () => {
  it('should return undefined if webhookNotification not present', async () => {
    //given
    const endpoint = 'testEndpoint';
    const subscriptionEndpoints = `
    <#websocketNotification> <http://www.w3.org/ns/solid/notifications#subscription> <${endpoint}>.
    `;

    const result = await __getSubscriptionUrl(subscriptionEndpoints);

    expect(result).toEqual(undefined);
  });

  it('should return subscription url if webhookNotification present', async () => {
    //given
    const endpoint = 'testEndpoint';
    const subscriptionEndpoints = `
    <#webhookNotification> <http://www.w3.org/ns/solid/notifications#subscription> <${endpoint}>.
    <http://example.com/.well-known/solid> a <http://www.w3.org/ns/pim/space#Storage>;
        <http://www.w3.org/ns/solid/notifications#subscription> <http://example.com/.notifications/WebSocketChannel2023/>, <${endpoint}>.
    <http://example.com/.notifications/WebSocketChannel2023/> <http://www.w3.org/ns/solid/notifications#channelType> <http://www.w3.org/ns/solid/notifications#WebSocketChannel2023>;
        <http://www.w3.org/ns/solid/notifications#feature> <http://www.w3.org/ns/solid/notifications#accept>, <http://www.w3.org/ns/solid/notifications#endAt>, <http://www.w3.org/ns/solid/notifications#rate>, <http://www.w3.org/ns/solid/notifications#startAt>, <http://www.w3.org/ns/solid/notifications#state>.
    <${endpoint}> <http://www.w3.org/ns/solid/notifications#channelType> <http://www.w3.org/ns/solid/notifications#WebHookChannel2023>;
        <http://www.w3.org/ns/solid/notifications#feature> <http://www.w3.org/ns/solid/notifications#accept>, <http://www.w3.org/ns/solid/notifications#endAt>, <http://www.w3.org/ns/solid/notifications#rate>, <http://www.w3.org/ns/solid/notifications#startAt>, <http://www.w3.org/ns/solid/notifications#state>.
    `;

    const result = await __getSubscriptionUrl(subscriptionEndpoints);

    expect(result).toEqual(endpoint);
  });

});

describe('getBaseUrl', () => {
  it('should return base url', async () => {
    //given
    const url = 'http://test.server:53642/root/path/resource';
    const expectedResult = 'http://test.server:53642/';

    const result = __getBaseUrl(url);

    expect(result).toEqual(expectedResult);
  });
});

describe('removeDataRequestLink', () => {
  it('should delete link resource ', async () => {
    const authFetchMock = jest.fn();
    authFetchMock.mockResolvedValue({ status: 204 });

    //given url
    const resourceUrl = 'http://link.url/resource';

    await __removeDataRequestLink(resourceUrl, authFetchMock);

    const args = authFetchMock.mock.lastCall;
    expect(args).toHaveLength(2);
    expect(args[0]).toBe(resourceUrl);
    expect(args[1].method).toBe('DELETE');
  });
});

describe('fetchCredentialSubject', () => {
  it('should call fetchInskrivningStatusMock in TEST ', async () => {
    const expectedResult = {
      type: 'JobSeekerRegistrationStatus',
      subject: 'test',
      isRegistered: false,
      registrationDate: 'test',
    };

    const p = __fetchCredentialSubject('JobSeekerRegistrationStatus', 'test', true);

    await expect(p).resolves.toEqual(expectedResult);
  });

  it('should call the right AIS feetch function ', async () => {
    const expectedResult = {
      type: 'JobSeekerRegistrationStatus',
      subject: '121212121212',
      isRegistered: true,
      registrationDate: '2023-01-04',
    };
    const inskrivningStatusFetcherMock = jest.fn();
    inskrivningStatusFetcherMock.mockRejectedValueOnce({
      'arbetssokande_status': 'Aktuell',
      'inskrivningsdatum': '2023-01-04',
      'sokandekategori_kod': '11',
    });
    const registrationSubjectFetcherMock = jest.fn();
    registrationSubjectFetcherMock.mockResolvedValueOnce(expectedResult);
    const relevanceStatusSubjectFetcherMock = jest.fn();

    const p = __fetchCredentialSubject('JobSeekerRegistrationStatus', '121212121212', false, inskrivningStatusFetcherMock, registrationSubjectFetcherMock, relevanceStatusSubjectFetcherMock);

    await expect(p).resolves.toEqual(expectedResult);
    expect(registrationSubjectFetcherMock).toBeCalledTimes(1);
    expect(relevanceStatusSubjectFetcherMock).toBeCalledTimes(0);
  });

  it('should call the right AIS fetch function ', async () => {
    const expectedResult = {
      type: 'InternshipRelevanceStatus',
      subject: '121212121212',
      isRegistered: false,
      registrationDate: '',
    };
    const inskrivningStatusFetcherMock = jest.fn();
    inskrivningStatusFetcherMock.mockResolvedValueOnce({
      'arbetssokande_status': 'Avskriven',
      'inskrivningsdatum': '',
      'sokandekategori_kod': '23',
    });
    const registrationSubjectFetcherMock = jest.fn();
    const relevanceStatusSubjectFetcherMock = jest.fn();
    relevanceStatusSubjectFetcherMock.mockResolvedValueOnce(expectedResult);

    const p = __fetchCredentialSubject('InternshipRelevanceStatus', '121212121212', false, inskrivningStatusFetcherMock, registrationSubjectFetcherMock, relevanceStatusSubjectFetcherMock);

    await expect(p).resolves.toEqual(expectedResult);
    expect(registrationSubjectFetcherMock).toBeCalledTimes(0);
    expect(relevanceStatusSubjectFetcherMock).toBeCalledTimes(1);
  });

});

describe('getObjectsForPredicate', () => {
  it('should return empty collection if given predicate not found', async () => {
    //given
    const predicate = 'predicate3';
    const data = `
<> a <Resource>;
  <predicate1> <a>, <c>, <b>;
  <predicate2> <d>;
  <predicate1> <e>.
`;
    const result = __getObjectsForPredicate(data, predicate);

    expect(result).toEqual([]);
  });

  it('should return collection of resources with a given predicate', async () => {
    //given
    const predicate = 'predicate1';
    const data = `
<> a <Resource>;
  <predicate1> <a>, <c>, <b>;
  <predicate2> <d>;
  <predicate1> <e>.
`;

    const result = __getObjectsForPredicate(data, predicate);

    expect(result).toEqual([ 'a', 'c', 'b', 'e']);
  });

});

describe('put', () => {
  it('should put resource content to given url', async () => {
    const authFetchMock = jest.fn();
    //given url
    const resourceUrl = 'http://link.url/resource';
    const resourceData = 'testdata';

    await __put(resourceUrl, resourceData, authFetchMock);

    const args = authFetchMock.mock.lastCall;
    expect(args).toHaveLength(2);
    expect(args[0]).toBe(resourceUrl);
    expect(args[1].method).toBe('PUT');
    expect(args[1].body).toBe(resourceData);
  });

  it('should use headers if given', async () => {
    const authFetchMock = jest.fn();
    //given url
    const resourceUrl = 'http://link.url/resource';
    const resourceData = '{"test":"data"}';
    const headers = { 'Content-Type': 'application/json' };

    await __put(resourceUrl, resourceData, authFetchMock, { 'Content-Type': 'application/json' });

    const args = authFetchMock.mock.lastCall;
    expect(args).toHaveLength(2);
    expect(args[0]).toBe(resourceUrl);
    expect(args[1].method).toBe('PUT');
    expect(args[1].body).toBe(resourceData);
    expect(args[1].headers).toStrictEqual(headers);
  });

});

describe('initializeEgendata', () => {

  const storeFunction: jest.Mocked<StoreFunction> = jest.fn();

  //given
  const webId = 'http://idp/testuser/profile#me';
  const storagePodUrl = 'http://pod/testuser/';

  it('should create egendata resources in the pod storage', async () => {
    await __initializeEgendata(webId, storagePodUrl, storeFunction);

    expect(storeFunction).toBeCalledTimes(3);
    //expect(storeFunction).nthCalledWith(1, `${storagePodUrl}egendata/inbox/.acl`);
  });
});

describe('scanInboxWithNewToken', () => {
  const getAuthFetchMock = jest.fn();
  getAuthFetchMock
    .mockReturnValueOnce('authFetch1')
    .mockReturnValueOnce('authFetch2');
    
  const authFetchFactoryMock = { getAuthFetch: getAuthFetchMock };
  const containerDataFetcherMock = jest.fn();
  const containerScannerMock = jest.fn();

  //given
  const containerUrl = 'http://pod/testuser/container/';

  it('should fetch container data with a new authFetchFunction for every call', async () => {
    const p1 = scanInboxWithNewToken(containerUrl, 'key', authFetchFactoryMock, containerDataFetcherMock, containerScannerMock);
    const p2 = scanInboxWithNewToken(containerUrl, 'key', authFetchFactoryMock, containerDataFetcherMock, containerScannerMock);

    await p1;
    await p2;
    const p1Fetch = containerDataFetcherMock.mock.calls[0][1];
    const p2Fetch = containerDataFetcherMock.mock.calls[1][1];

    expect(p1Fetch).not.toEqual(p2Fetch);
  });
});

describe('lockedContainerResourceHandler', () => {
  //given
  const containerUrl = 'http://pod/testuser/container/';
  const input = {
    resourceUri: containerUrl,
    key: 'key',
  };

  it('should call handler function if resource not being processed', async () => {
    const handlerMock = jest.fn();    
    const resourceLock = new Set<string>();
    const expectedResult = 'Ok';
    const expectedReturnValue = {
      result: expectedResult,
      status: 200,
    };
    handlerMock
      .mockReturnValueOnce(expectedReturnValue);

    const p = lockedContainerResourceHandler(input, handlerMock, resourceLock);

    await expect(p).resolves.toEqual(expectedReturnValue);
    expect(handlerMock).toBeCalledTimes(1);
  });

  it('should not call handler function if resource is being processed', async () => {
    const handlerMock = jest.fn();    
    const resourceLock = new Set<string>();
    resourceLock.add(input.resourceUri);

    const expectedResult = `Resource ${input.resourceUri} is already being handeled`;
    const expectedReturnValue = {
      result: expectedResult,
      status: 200,
    };
    handlerMock
      .mockReturnValueOnce(expectedReturnValue);

    const p = lockedContainerResourceHandler(input, handlerMock, resourceLock);

    await expect(p).resolves.toEqual(expectedReturnValue);
    expect(handlerMock).not.toBeCalled();
  });

});

describe('inboxHandler', () => {
  //given
  const handlerMock = jest.fn();    
  const containerUrl = 'http://pod/testuser/container/';
  const input = {
    resourceUri: containerUrl,
    key: 'key',
  };

  it('should call handler function', async () => {
    await inboxHandler(input, handlerMock);
    expect(handlerMock).toBeCalled();
  });
});

describe('inboxHandlerImpl', () => {
  const getMocks = (authFetchMock: any, dataRequest: any = { documentType: 'Dummy' }) => {
    const inboxResourceParserMock = jest.fn();
    inboxResourceParserMock.mockReturnValue({ outboundDataRequest: 'test' });
    const dataRequestLinkRemovalMock = jest.fn();
    const dataRequestParserMock = jest.fn();
    dataRequestParserMock.mockReturnValue(dataRequest);
    const getAuthFetchMock = jest.fn();
    getAuthFetchMock.mockReturnValue(authFetchMock);
    const fecthFactoryMock = {
      getAuthFetch: getAuthFetchMock,
    };
    const auditLoggerMock = {
      incoming: jest.fn(),
      outgoing: jest.fn(),
    };
    const effectMock = {
      fetchFactory: fecthFactoryMock,
      audit: auditLoggerMock,
    };
    const containerUrl = 'http://pod/testuser/container/';
    const input = {
      resourceUri: containerUrl,
      key: 'key',
    };

    return { input, effectMock, inboxResourceParserMock, dataRequestLinkRemovalMock, dataRequestParserMock };
  };

  it('returns an error of document type can not be handled', async () => {
    const requestLink = 'http://pod/testuser/request';
    const outboundDataRequestData = 'outboundDataRequestData';
    const authFetchMock = jest.fn();
    authFetchMock
      .mockResolvedValueOnce({ text: jest.fn(() => (requestLink)) })
      .mockResolvedValueOnce({ text: jest.fn(() => (outboundDataRequestData)), ok: true });

    const { input, effectMock, inboxResourceParserMock, dataRequestLinkRemovalMock, dataRequestParserMock } = getMocks(authFetchMock);
    const p = inboxHandlerImpl(input, effectMock, inboxResourceParserMock, dataRequestLinkRemovalMock, dataRequestParserMock);
    
    await expect(p).resolves.toEqual({ status: 404, result: 'Document type Dummy can not be handeled.' });
    expect(inboxResourceParserMock).toBeCalledWith(requestLink);
    expect(dataRequestParserMock).toBeCalledTimes(1);
  });

  it('returns status [200 OK] for fetch errors 403, 404 and 410', async () => {
    const requestLink = 'http://pod/testuser/request';
    const outboundDataRequestData = 'outboundDataRequestData';
    const authFetchMock = jest.fn();
    authFetchMock
      .mockResolvedValueOnce({ text: jest.fn(() => (requestLink)), ok: true })
      .mockResolvedValueOnce({ text: jest.fn(() => (outboundDataRequestData)), ok: false, status: 403 })
      .mockResolvedValueOnce({ ok: true });

    const { input, effectMock, inboxResourceParserMock, dataRequestLinkRemovalMock, dataRequestParserMock } = getMocks(authFetchMock);
    const p = inboxHandlerImpl(input, effectMock, inboxResourceParserMock, dataRequestLinkRemovalMock, dataRequestParserMock);
    
    await expect(p).resolves.toEqual({ status: 200, result: 'OK' });
    expect(inboxResourceParserMock).toBeCalledWith(requestLink);
    expect(dataRequestParserMock).toBeCalledTimes(0);
  });

  it('returns status [400 ERROR] for other errors', async () => {
    const requestLink = 'http://pod/testuser/request';
    const outboundDataRequestData = 'outboundDataRequestData';
    const authFetchMock = jest.fn();
    authFetchMock
      .mockResolvedValueOnce({ text: jest.fn(() => (requestLink)), ok: true })
      .mockResolvedValueOnce({ text: jest.fn(() => (outboundDataRequestData)), ok: false, status: 500 })
      .mockResolvedValueOnce({ ok: true });

    const { input, effectMock, inboxResourceParserMock, dataRequestLinkRemovalMock, dataRequestParserMock } = getMocks(authFetchMock);
    const p = inboxHandlerImpl(input, effectMock, inboxResourceParserMock, dataRequestLinkRemovalMock, dataRequestParserMock);
    
    await expect(p).resolves.toEqual({ status: 400, result: 'ERROR' });
    expect(inboxResourceParserMock).toBeCalledWith(requestLink);
    expect(dataRequestParserMock).toBeCalledTimes(0);
  });

  it('returns status [200 OK] if data subject identifier does not match expected format, while audit logging incoming and outgoing', async () => {
    const requestLink = 'http://pod/testuser/request';
    const outboundDataRequestData = 'outboundDataRequestData';
    const authFetchMock = jest.fn();
    authFetchMock
      .mockResolvedValueOnce({ text: jest.fn(() => (requestLink)), ok: true })
      .mockResolvedValueOnce({ text: jest.fn(() => (outboundDataRequestData)), ok: true, status: 200 })
      .mockResolvedValueOnce({ ok: true });
    const pnr = '1111111111';
    const dataRequest = { documentType: `${egendataPrefix}JobSeekerRegistrationStatus`, dataSubjectIdentifier: pnr };
    const { input, effectMock, inboxResourceParserMock, dataRequestLinkRemovalMock, dataRequestParserMock } = getMocks(authFetchMock, dataRequest);
    const p = inboxHandlerImpl(input, effectMock, inboxResourceParserMock, dataRequestLinkRemovalMock, dataRequestParserMock);
    
    await expect(p).resolves.toEqual({ status: 200, result: 'OK' });
    expect(inboxResourceParserMock).toBeCalledWith(requestLink);
    expect(dataRequestParserMock).toBeCalledTimes(1);
    expect(effectMock.audit.incoming).toBeCalledTimes(1);
    expect(effectMock.audit.outgoing).toBeCalledTimes(1);  
    expect(effectMock.audit.incoming).toBeCalledWith(pnr, 'JobSeekerRegistrationStatus');
    expect(effectMock.audit.outgoing).toBeCalledWith(pnr, 0);  
  });

  it('fetches an AIS resource, creates a VC, writes the VC and removes the link if subject format OK', async () => {
    const requestLink = 'http://pod/testuser/request';
    const outboundDataRequestData = 'outboundDataRequestData';
    const authFetchMock = jest.fn();
    authFetchMock
      .mockResolvedValueOnce({ text: jest.fn(() => (requestLink)), ok: true })
      .mockResolvedValueOnce({ text: jest.fn(() => (outboundDataRequestData)), ok: true, status: 200 })
      .mockResolvedValueOnce({ ok: true });
    const credentialSubjectFetcherMock = jest.fn();
    credentialSubjectFetcherMock
      .mockResolvedValueOnce({ isRegistered: true });
    const verifiableCredentialIssuerMock = jest.fn();
    const verifiableCredentialWriterMock = jest.fn();
    verifiableCredentialWriterMock.mockResolvedValueOnce({ ok: true });
    const pnr = '111111111111';
    const dataRequest = { documentType: `${egendataPrefix}JobSeekerRegistrationStatus`, dataSubjectIdentifier: pnr };
    const { input, effectMock, inboxResourceParserMock, dataRequestLinkRemovalMock, dataRequestParserMock } = getMocks(authFetchMock, dataRequest);
    const p = inboxHandlerImpl(
      input,
      effectMock,
      inboxResourceParserMock,
      dataRequestLinkRemovalMock,
      dataRequestParserMock,
      credentialSubjectFetcherMock,
      verifiableCredentialIssuerMock,
      verifiableCredentialWriterMock);
    
    await expect(p).resolves.toEqual({ status: 200, result: 'OK' });
    expect(inboxResourceParserMock).toBeCalledWith(requestLink);
    expect(dataRequestParserMock).toBeCalledTimes(1);
    expect(credentialSubjectFetcherMock).toBeCalledTimes(1);
    expect(verifiableCredentialIssuerMock).toBeCalledTimes(1);
    expect(verifiableCredentialWriterMock).toBeCalledTimes(1);
    expect(dataRequestLinkRemovalMock).toBeCalledTimes(1);
  });

  it('returns an error if a JobSeekerRegistrationCertificate is requested and the user is not enlisted', async () => {
    const requestLink = 'http://pod/testuser/request';
    const outboundDataRequestData = 'outboundDataRequestData';
    const authFetchMock = jest.fn();
    authFetchMock
      .mockResolvedValueOnce({ text: jest.fn(() => (requestLink)), ok: true })
      .mockResolvedValueOnce({ text: jest.fn(() => (outboundDataRequestData)), ok: true, status: 200 })
      .mockResolvedValueOnce({ ok: true });
    const credentialSubjectFetcherMock = jest.fn();
    credentialSubjectFetcherMock
      .mockResolvedValueOnce({ isRegistered: false });
    const verifiableCredentialIssuerMock = jest.fn();
    const verifiableCredentialWriterMock = jest.fn();
    const responseLinkWriterMock = jest.fn();
    const errorWriterMock = jest.fn();
    errorWriterMock
      .mockResolvedValueOnce({ ok: true });
    const pnr = '111111111111';
    const dataRequest = { documentType: `${egendataPrefix}JobSeekerRegistrationCertificate`, dataSubjectIdentifier: pnr };
    const { input, effectMock, inboxResourceParserMock, dataRequestLinkRemovalMock, dataRequestParserMock } = getMocks(authFetchMock, dataRequest);
    const p = inboxHandlerImpl(
      input,
      effectMock,
      inboxResourceParserMock,
      dataRequestLinkRemovalMock,
      dataRequestParserMock,
      credentialSubjectFetcherMock,
      verifiableCredentialIssuerMock,
      verifiableCredentialWriterMock,
      responseLinkWriterMock,
      errorWriterMock);
    
    await expect(p).resolves.toEqual({ status: 200, result: 'OK' });
    expect(inboxResourceParserMock).toBeCalledWith(requestLink);
    expect(dataRequestParserMock).toBeCalledTimes(1);
    expect(credentialSubjectFetcherMock).toBeCalledTimes(1);
    expect(verifiableCredentialIssuerMock).toBeCalledTimes(0);
    expect(verifiableCredentialWriterMock).toBeCalledTimes(0);
    expect(errorWriterMock).toBeCalledTimes(1);
    expect(dataRequestLinkRemovalMock).toBeCalledTimes(1);
  });

  it('returns status [200 OK] and removes data link request if VC storage erros with 403', async () => {
    const requestLink = 'http://pod/testuser/request';
    const outboundDataRequestData = 'outboundDataRequestData';
    const authFetchMock = jest.fn();
    authFetchMock
      .mockResolvedValueOnce({ text: jest.fn(() => (requestLink)), ok: true })
      .mockResolvedValueOnce({ text: jest.fn(() => (outboundDataRequestData)), ok: true, status: 200 })
      .mockResolvedValueOnce({ ok: false, status: 403 });
    const credentialSubjectFetcherMock = jest.fn();
    credentialSubjectFetcherMock
      .mockResolvedValueOnce({ isRegistered: true });
    const verifiableCredentialIssuerMock = jest.fn();
    const verifiableCredentialWriterMock = jest.fn();
    verifiableCredentialWriterMock.mockResolvedValueOnce({ ok: false, status: 403 });
    const responseLinkWriterMock = jest.fn();
    const pnr = '111111111111';
    const dataRequest = { documentType: `${egendataPrefix}JobSeekerRegistrationStatus`, dataSubjectIdentifier: pnr };
    const { input, effectMock, inboxResourceParserMock, dataRequestLinkRemovalMock, dataRequestParserMock } = getMocks(authFetchMock, dataRequest);
    const p = inboxHandlerImpl(
      input,
      effectMock,
      inboxResourceParserMock,
      dataRequestLinkRemovalMock,
      dataRequestParserMock,
      credentialSubjectFetcherMock,
      verifiableCredentialIssuerMock,
      verifiableCredentialWriterMock,
      responseLinkWriterMock);
    
    await expect(p).resolves.toEqual({ status: 200, result: 'OK' });
    expect(inboxResourceParserMock).toBeCalledWith(requestLink);
    expect(dataRequestParserMock).toBeCalledTimes(1);
    expect(credentialSubjectFetcherMock).toBeCalledTimes(1);
    expect(verifiableCredentialIssuerMock).toBeCalledTimes(1);
    expect(verifiableCredentialWriterMock).toBeCalledTimes(1);
    expect(dataRequestLinkRemovalMock).toBeCalledTimes(1);
    expect(responseLinkWriterMock).toBeCalledTimes(0);
  });

  it('returns status [400 ERROR] if VC storage erros with other then 403', async () => {
    const requestLink = 'http://pod/testuser/request';
    const outboundDataRequestData = 'outboundDataRequestData';
    const authFetchMock = jest.fn();
    authFetchMock
      .mockResolvedValueOnce({ text: jest.fn(() => (requestLink)), ok: true })
      .mockResolvedValueOnce({ text: jest.fn(() => (outboundDataRequestData)), ok: true, status: 200 })
      .mockResolvedValueOnce({ ok: false, status: 500 });
    const credentialSubjectFetcherMock = jest.fn();
    credentialSubjectFetcherMock
      .mockResolvedValueOnce({ isRegistered: true });
    const verifiableCredentialIssuerMock = jest.fn();
    const verifiableCredentialWriterMock = jest.fn();
    verifiableCredentialWriterMock.mockResolvedValueOnce({ ok: false, status: 409 });
    const responseLinkWriterMock = jest.fn();
    const pnr = '111111111111';
    const dataRequest = { documentType: `${egendataPrefix}JobSeekerRegistrationStatus`, dataSubjectIdentifier: pnr };
    const { input, effectMock, inboxResourceParserMock, dataRequestLinkRemovalMock, dataRequestParserMock } = getMocks(authFetchMock, dataRequest);
    const p = inboxHandlerImpl(
      input,
      effectMock,
      inboxResourceParserMock,
      dataRequestLinkRemovalMock,
      dataRequestParserMock,
      credentialSubjectFetcherMock,
      verifiableCredentialIssuerMock,
      verifiableCredentialWriterMock,
      responseLinkWriterMock);
    
    await expect(p).resolves.toEqual({ status: 400, result: 'ERROR' });
    expect(inboxResourceParserMock).toBeCalledWith(requestLink);
    expect(dataRequestParserMock).toBeCalledTimes(1);
    expect(credentialSubjectFetcherMock).toBeCalledTimes(1);
    expect(verifiableCredentialIssuerMock).toBeCalledTimes(1);
    expect(verifiableCredentialWriterMock).toBeCalledTimes(1);
    expect(dataRequestLinkRemovalMock).toBeCalledTimes(0);
    expect(responseLinkWriterMock).toBeCalledTimes(0);
  });

  it('returns status [200 OK] and removes data link request if responseLink storage erros with 403', async () => {
    const requestLink = 'http://pod/testuser/request';
    const outboundDataRequestData = 'outboundDataRequestData';
    const authFetchMock = jest.fn();
    authFetchMock
      .mockResolvedValueOnce({ text: jest.fn(() => (requestLink)), ok: true })
      .mockResolvedValueOnce({ text: jest.fn(() => (outboundDataRequestData)), ok: true, status: 200 })
      .mockResolvedValueOnce({ ok: false, status: 403 });
    const credentialSubjectFetcherMock = jest.fn();
    credentialSubjectFetcherMock
      .mockResolvedValueOnce({ isRegistered: true });
    const verifiableCredentialIssuerMock = jest.fn();
    const verifiableCredentialWriterMock = jest.fn();
    verifiableCredentialWriterMock.mockResolvedValueOnce({ ok: true });
    const responseLinkWriterMock = jest.fn();
    responseLinkWriterMock.mockResolvedValueOnce({ ok: false, status: 403 });
    const pnr = '111111111111';
    const dataRequest = { documentType: `${egendataPrefix}JobSeekerRegistrationStatus`, dataSubjectIdentifier: pnr };
    const { input, effectMock, inboxResourceParserMock, dataRequestLinkRemovalMock, dataRequestParserMock } = getMocks(authFetchMock, dataRequest);
    const p = inboxHandlerImpl(
      input,
      effectMock,
      inboxResourceParserMock,
      dataRequestLinkRemovalMock,
      dataRequestParserMock,
      credentialSubjectFetcherMock,
      verifiableCredentialIssuerMock,
      verifiableCredentialWriterMock,
      responseLinkWriterMock);
    
    await expect(p).resolves.toEqual({ status: 200, result: 'OK' });
    expect(inboxResourceParserMock).toBeCalledWith(requestLink);
    expect(dataRequestParserMock).toBeCalledTimes(1);
    expect(credentialSubjectFetcherMock).toBeCalledTimes(1);
    expect(verifiableCredentialIssuerMock).toBeCalledTimes(1);
    expect(verifiableCredentialWriterMock).toBeCalledTimes(1);
    expect(dataRequestLinkRemovalMock).toBeCalledTimes(1);
    expect(responseLinkWriterMock).toBeCalledTimes(1);
  });

  it('returns status [400 ERROR] if responseLink storage  erros with other then 403', async () => {
    const requestLink = 'http://pod/testuser/request';
    const outboundDataRequestData = 'outboundDataRequestData';
    const authFetchMock = jest.fn();
    authFetchMock
      .mockResolvedValueOnce({ text: jest.fn(() => (requestLink)), ok: true })
      .mockResolvedValueOnce({ text: jest.fn(() => (outboundDataRequestData)), ok: true, status: 200 })
      .mockResolvedValueOnce({ ok: false, status: 500 });
    const credentialSubjectFetcherMock = jest.fn();
    credentialSubjectFetcherMock
      .mockResolvedValueOnce({ isRegistered: true });
    const verifiableCredentialIssuerMock = jest.fn();
    const verifiableCredentialWriterMock = jest.fn();
    verifiableCredentialWriterMock.mockResolvedValueOnce({ ok: true });
    const responseLinkWriterMock = jest.fn();
    responseLinkWriterMock.mockResolvedValueOnce({ ok: false, status: 409 });
    const pnr = '111111111111';
    const dataRequest = { documentType: `${egendataPrefix}JobSeekerRegistrationStatus`, dataSubjectIdentifier: pnr };
    const { input, effectMock, inboxResourceParserMock, dataRequestLinkRemovalMock, dataRequestParserMock } = getMocks(authFetchMock, dataRequest);
    const p = inboxHandlerImpl(
      input,
      effectMock,
      inboxResourceParserMock,
      dataRequestLinkRemovalMock,
      dataRequestParserMock,
      credentialSubjectFetcherMock,
      verifiableCredentialIssuerMock,
      verifiableCredentialWriterMock,
      responseLinkWriterMock);
    
    await expect(p).resolves.toEqual({ status: 400, result: 'ERROR' });
    expect(inboxResourceParserMock).toBeCalledWith(requestLink);
    expect(dataRequestParserMock).toBeCalledTimes(1);
    expect(credentialSubjectFetcherMock).toBeCalledTimes(1);
    expect(verifiableCredentialIssuerMock).toBeCalledTimes(1);
    expect(verifiableCredentialWriterMock).toBeCalledTimes(1);
    expect(dataRequestLinkRemovalMock).toBeCalledTimes(0);
    expect(responseLinkWriterMock).toBeCalledTimes(1);
  });

});
