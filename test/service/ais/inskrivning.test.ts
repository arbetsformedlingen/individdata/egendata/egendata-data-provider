import { fetchInskrivningStatus } from '../../../src/service/ais/inskrivning';

describe('fetchInskrivningStatus', () => {
  it('should call the fetch function with expected headers and proxy settings', () => {
  // given
    const pnr = '111111111111';
    const expectedInskrivningsStatus = {
      'arbetssokande_status': '',
      'inskrivningsdatum': '',
      'sokandekategori_kod': '',
    };
    const fetcherMock = jest.fn();
    fetcherMock.mockResolvedValueOnce({ json: jest.fn(() => (expectedInskrivningsStatus)), ok: true });

    const p = fetchInskrivningStatus(pnr, fetcherMock);

    expect(p).resolves.toEqual(expectedInskrivningsStatus);
    expect(fetcherMock).toBeCalledTimes(1);
    // TODO: add better expectations
  });
});

