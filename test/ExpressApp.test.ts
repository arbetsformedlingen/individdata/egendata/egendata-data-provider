import request from 'supertest';
import { ExpressApp } from '../src/ExpressApp';

jest.mock('../src/util/vc', () => ({
  __esModule: true,
  loadKey: jest.fn(),
}));

describe('GET /random-url', () => {
  it('should return 404', (done) => {
    const expressApp = new ExpressApp();
    request(expressApp.getApp()).get('/reset')
      .expect(404, done);
  });
});
