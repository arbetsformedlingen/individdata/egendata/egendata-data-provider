import { inboxHandler } from '../util/solid';
import { logger } from '../util/logger';
export class WebhookController {
  private readonly key: any;

  constructor(key: any) {
    this.key = key;
  }

  async handle(request: any, response: any) {
    const payload = request.body;
    logger.debug('Received webhook %o', payload);
    const { object: resourceUri, type, '@context': context } = payload;
    logger.info(`Received ${type[0]} notification for ${resourceUri}`);

    try {
      if (!context.includes('https://www.w3.org/ns/solid/notification/v1') || !type.includes('Add')) {
        logger.debug('Ignoring webhook message, because it\'s not of type \'https://www.w3.org/ns/activitystreams#Add\'...');
        response.status(200).send('OK');
        return;
      }
    } catch (err) {
      // NOP
    }

    try {
      response.status(200).end();
      const key = this.key;
      const handleInboxResourceResponse = await inboxHandler({ resourceUri, key });
      if (handleInboxResourceResponse.status === 200) {
        logger.info('Successfully handled webhook');
      } else {
        logger.error(`Failed to handle webhook, ${handleInboxResourceResponse.result}`);
      }
  
    } catch (err) {
      logger.error(`Error when handling webhook: ${err}`);
    }
  }
}
