import http from 'http';
import { ExpressApp } from './ExpressApp';
import { unsubscribe } from './util/solid';
import { logger } from './util/logger';
import { defaultAuthFetchFactory } from './util/OidcAuthFetchFactory';

export class Server {
  private readonly expressApp: ExpressApp;

  private httpServer?: http.Server;

  private subscription?: string;

  // eslint-disable-next-line @typescript-eslint/no-empty-function
  public constructor(expressApp: ExpressApp) {
    this.expressApp = expressApp;
  }

  public async getHttpServer(): Promise<http.Server> {
    if (!this.httpServer) {
      const app = this.expressApp.getApp();
      const httpServer = http.createServer(app);
      const authFetch = await defaultAuthFetchFactory.getAuthFetch();
      const retval = await this.expressApp.initialize(authFetch);
      this.subscription = retval.unsubscribeEndpoint;
      this.httpServer = httpServer;
    }
    return this.httpServer;
  }

  public async terminate() {
    try {
      if (this.subscription) {
        const authFetch = await defaultAuthFetchFactory.getAuthFetch();
        await unsubscribe(this.subscription, authFetch);
        logger.info('Unsubscribed from notifications');
      }
    } catch (err) {
      logger.error(err);
    } finally {
      const httpServer = await this.getHttpServer();
      httpServer.close(() => {
        logger.info('HTTP server closed');
      });
    }
  }
}
