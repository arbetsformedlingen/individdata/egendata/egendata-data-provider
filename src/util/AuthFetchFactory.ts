import { KeyPair } from '@inrupt/solid-client-authn-core';

export type TokenKeyPair = {
  accessToken: string,
  dpopKey: KeyPair,
};

export type AuthFetchFunction = typeof fetch;

export interface AuthFetchFactory {
  getAuthFetch(env?: string): Promise<AuthFetchFunction>;
}