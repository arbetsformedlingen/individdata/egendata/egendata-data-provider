import https from 'https';
import http from 'http';
import nodeFetch from 'node-fetch';
import { buildAuthenticatedFetch, createDpopHeader, generateDpopKeyPair } from '@inrupt/solid-client-authn-core';
import { AuthFetchFactory, AuthFetchFunction, TokenKeyPair } from './AuthFetchFactory';
import { clientId, clientSecret, identityProviderBaseUrl } from '../config';
import { logger } from './logger';


export class OidcAuthFetchFactory implements AuthFetchFactory {

  async getAuthFetch(env = process.env.NODE_ENV, id = clientId, secret = clientSecret, accessTokenKeyFactory = this.fetchAccessTokenKey, authenticatedFetchBuilder = buildAuthenticatedFetch): Promise<AuthFetchFunction> {
    if (env === 'test') return nodeFetch as unknown as AuthFetchFunction;

    const { accessToken, dpopKey } = await accessTokenKeyFactory(id, secret);
    const authFetchFunction = await authenticatedFetchBuilder(nodeFetch as unknown as typeof fetch, accessToken, { dpopKey });
    return authFetchFunction;
  }

  async fetchAccessTokenKey(myId: string, mySecret: string, identityServerUrl = identityProviderBaseUrl, keyPairGenerator = generateDpopKeyPair, headerCreator = createDpopHeader, fetchFunction = nodeFetch): Promise<TokenKeyPair> {
    logger.debug('Fetching access token ...');
    const dpopKey = await keyPairGenerator();
    const authString = `${encodeURIComponent(myId)}:${encodeURIComponent(mySecret)}`;
    const isHttps = identityServerUrl.startsWith('https:');
    const tokenUrl = `${identityServerUrl}/.oidc/token`;
    const agent = isHttps ? new https.Agent({
      rejectUnauthorized: false,
    }) : new http.Agent();
    const response = await fetchFunction(tokenUrl, {
      method: 'POST',
      headers: {
        // The header needs to be in base64 encoding.
        authorization: `Basic ${Buffer.from(authString).toString('base64')}`,
        'content-type': 'application/x-www-form-urlencoded',
        dpop: await headerCreator(tokenUrl, 'POST', dpopKey),
      },
      agent,
      body: new URLSearchParams({
        grant_type: 'client_credentials',
        scope: 'webid',
      }),
    });

    if (!response.ok) {
      // eslint-disable-next-line @typescript-eslint/naming-convention
      const { error, error_description } = await response.json() as any;
      throw new Error(`Could not fetch access token: ${error}, ${error_description}`);
    }

    const data = await response.json() as any;
    // eslint-disable-next-line @typescript-eslint/naming-convention
    const { access_token } = data;
    return { accessToken: access_token, dpopKey: dpopKey };
  }
}

const lazyFetchFactory = () => {
  let factory: AuthFetchFactory | undefined = undefined;

  return {
    getAuthFetch: async (): Promise<typeof fetch> => {
      if (!factory) {
        factory = new OidcAuthFetchFactory();
      }
      return factory.getAuthFetch();
    },
  };
};

export const defaultAuthFetchFactory: AuthFetchFactory = lazyFetchFactory();
