import { pino } from 'pino';
import { options } from '../logger.options';

export const logger = pino(options);

export type { Logger } from 'pino';

//export default logger;
