import N3 from 'n3';
import { issueVerifiableCredential, VerifiableCredentialIssuer } from './vc';
import { KeyPair } from '@inrupt/solid-client-authn-core';
import { v4 as uuid } from 'uuid';
import { baseUrl, webid, useAisMock } from '../config';
import { fetchInskrivningStatus, InskrivningStatus } from '../service/ais/inskrivning';
import { fetchInskrivningStatus as fetchInskrivningStatusMock } from '../service/ais/inskrivningMock';
import { fetchRegistrationStatusSubject, RegistrationStatusSubject, RegistrationStatusSubjectFetcher } from '../service/registrationStatus';
import { fetchQualificationStatusSubject, QualificationStatusSubject, QualificationStatusSubjectFetcher } from '../service/relevanceStatus';
import { logger } from './logger';
import { AuthFetchFactory, AuthFetchFunction } from './AuthFetchFactory';
import { defaultAuthFetchFactory } from './OidcAuthFetchFactory';
import { defaultAuditLogger } from '../logger/AuditLogger/defaultAuditLogger';
import { AuditLogger } from '../logger/AuditLogger/AuditLogger';

export const egendataPrefix = 'https://egendata.se/schema/core/v1#';

export const parseInboxResource = (data: string) => {
  logger.trace('Parsing from data: %o', data);
  const N3Parser = new N3.Parser();
  const store = new N3.Store(N3Parser.parse(data));
  const findPredicate = `${egendataPrefix}OutboundDataRequest`;
  logger.trace('Searching for predicate: %o', findPredicate);
  const foundObjects = store.getObjects('', findPredicate, '');

  if (foundObjects.length < 1) {
    throw new Error(`Did not find predicate "${findPredicate}" in data`);
  }

  logger.trace('Found objects: %o', foundObjects);
  const firstObject = foundObjects[0];
  const outboundDataRequest = firstObject.value;
  return { outboundDataRequest };
};

export const storageUrlFromProfile = (profile: string) => {
  const N3Parser = new N3.Parser();
  const st = new N3.Store(N3Parser.parse(profile));
  const storageUrl = st.getObjects(null, 'http://www.w3.org/ns/pim/space#storage', null)[0].value;
  return storageUrl;
};

export const parseDataRequestData = (data: string) => {
  const N3Parser = new N3.Parser();
  const store = new N3.Store(N3Parser.parse(data));
  const id = store.getObjects('', `${egendataPrefix}requestId`, '')[0]?.value;
  const documentType = store.getObjects('', `${egendataPrefix}documentType`, '')[0]?.value;
  const dataSubjectIdentifier = store.getObjects('', `${egendataPrefix}dataSubjectIdentifier`, '')[0]?.value;
  const dataLocation = store.getObjects('', `${egendataPrefix}dataLocation`, '')[0]?.value;
  const notificationInbox = store.getObjects('', `${egendataPrefix}notificationInbox`, '')[0]?.value;
  if (!id) logger.warn('Recieved data request missing id predicate');
  if (!documentType) logger.warn('Recieved data request missing documentType predicate');
  if (!dataSubjectIdentifier) logger.warn('Recieved data request missing dataSubjectIdentifier predicate');
  if (!dataLocation) logger.warn('Recieved data request missing dataLocation predicate');
  if (!notificationInbox) logger.warn('Recieved data request missing notificationInbox predicate');
  return { id, documentType, dataSubjectIdentifier, dataLocation, notificationInbox };
};

export type VerifiableCredentialWriter = (requestId: string, dataLocation: string, doc: any, authFetch: AuthFetchFunction) => Promise<Response>;

export const writeVerifiableCredential = async (requestId: string, dataLocation: string, doc: any, authFetch: AuthFetchFunction) => {
  const document = Buffer.from(JSON.stringify(doc), 'utf-8').toString('base64');
  const vcData = `@prefix egendata: <${egendataPrefix}> .
<> a egendata:InboundDataResponse ;
  egendata:requestId "${requestId}" ;
  egendata:providerWebId "${webid}" ;
  egendata:document "${document}" .
`;

  const response = await authFetch(dataLocation, {
    method: 'PUT',
    body: vcData,
    headers: { 'Content-Type': 'text/turtle' },
  });

  return response;
};

export type ResponseLinkWriter = (notificationInbox: string, dataLocation: string, authFetch: AuthFetchFunction, uuidx: () => string) => Promise<Response>;

export const saveLinkToInbox = async (notificationInbox: string, dataLocation: string, authFetch: AuthFetchFunction, uuidx: () => string) => {
  const linkData = `
@prefix egendata: <${egendataPrefix}> .
<> a egendata:InboundDataResponseLink ;
   egendata:InboundDataResponse <${dataLocation}> .
`;

  const url = `${notificationInbox}response-link-${uuidx()}`;
  const response = await authFetch(url, {
    method: 'PUT',
    body: linkData,
    headers: { 'Content-Type': 'text/turtle' },
  });
  
  return response;
};

export type ErrorWriter = (notificationInbox: string, requestId: string, requestUrl: string, errorId: string, errorMessage: string, authFetch: AuthFetchFunction, uuidx: () => string) => Promise<Response>;

export const writeErrorToInbox = async (notificationInbox: string, requestId: string, requestUrl: string, errorId: string, errorMessage: string, authFetch: AuthFetchFunction, uuidx: () => string) => {
  const linkData = `
@prefix egendata: <https://egendata.se/schema/core/v1#> .
<> a egendata:InboundDataResponseError ;
  egendata:requestId "${requestId}" ;
  egendata:requestUrl <${requestUrl}> ;
  egendata:errorId "${errorId}" ;
  egendata:errorMessage "${errorMessage}" .
`; 
  const url = `${notificationInbox}response-error-${uuidx()}`;
  const response = await authFetch(url, {
    method: 'PUT',
    body: linkData,
  });
  
  return response;
};

export const deleteResource = async (url: string, authFetch: AuthFetchFunction) => {
  const response = await authFetch(url, { method: 'DELETE' });
  return response;
};

export type TokenAndDpopKey = {
  accessToken: string,
  dpopKey: KeyPair,
};

const getSubscriptionUrl = (subscriptionEndpoints: any) => {

  const N3Parser = new N3.Parser();
  const store = new N3.Store(N3Parser.parse(subscriptionEndpoints));
  const obj = store.getSubjects('http://www.w3.org/ns/solid/notifications#channelType', 'http://www.w3.org/ns/solid/notifications#WebHookChannel2023', null);
  const subscriptionUrl = obj ? obj[0] ? obj[0].value : undefined : undefined;
  logger.trace('obj: %o', subscriptionUrl);
  return subscriptionUrl;
};

const getBaseUrl = (url: string) => {
  const { host, protocol } = new URL(url);
  return `${protocol}//${host}/`;
};

export const subscribeToInbox = async (storagePodUrl: string, authFetch: AuthFetchFunction) => {
  
  const baseURL = getBaseUrl(storagePodUrl);
  const solidUrl = `${baseURL}.well-known/solid`;

  // fectch subscription endpoint
  const response = await authFetch(solidUrl, {
    headers: {
      Accept: 'text/turtle', 
    },
  });

  const subscriptionEndpoints = await response.text();
  const subscriptionUrl = getSubscriptionUrl(subscriptionEndpoints);

  if (subscriptionUrl) {
    const data = {
      '@context': ['https://www.w3.org/ns/solid/notification/v1'],
      type: 'http://www.w3.org/ns/solid/notifications#WebHookChannel2023',
      topic: `${storagePodUrl}egendata/inbox/`,
      sendTo: `${baseUrl}/webhook`,
    };
    const body = JSON.stringify(data);
    const result = await authFetch(
      subscriptionUrl,
      {
        method: 'POST',
        body: body,
        headers: { 'Content-Type': 'application/ld+json' },
      },
    );
    if (!result.ok) {
      throw new Error('Subscription failed');
    }
    const json = await result.json();
    return  json.id;
  }
  throw new Error('No subscription URL found');
};

export const unsubscribe = async (url: string, authFetch: AuthFetchFunction ) => {
  await authFetch(url,
    {
      method: 'DELETE',
    },
  );
};

export type ContainerResourceHandlerResult = {
  status: number,
  result: string,
};

export type DataRequestLinkRemover = (resourceUri: string, authFetch: AuthFetchFunction) => Promise<void>;

const removeDataRequestLink = async (resourceUri: string, authFetch: AuthFetchFunction) => {
  const deleteResourceResponse = await deleteResource(resourceUri, authFetch);
  if (!deleteResourceResponse.ok)
    logger.error('Failed to delete link from inbox, error: %o', deleteResourceResponse.statusText);

  const { status } = deleteResourceResponse;
  logger.debug(`deleteResourceResponse[status: ${status}]`);
};

export type CredentialSubjectFetcher = (
  documentType: string,
  personnummer: string,
  mockAIS?: boolean,
  inskrivningStatusFetcher?: (personummer: string) => Promise<InskrivningStatus>,
  registrationStatusSubjectFetcher?: RegistrationStatusSubjectFetcher,
  relevanceStatusSubjectFetcher?: QualificationStatusSubjectFetcher) => Promise<RegistrationStatusSubject | QualificationStatusSubject>;

const fetchCredentialSubject: CredentialSubjectFetcher = async (
  documentType: string,
  personnummer: string,
  mockAIS = useAisMock,
  inskrivningStatusFetcher = fetchInskrivningStatus,
  registrationStatusSubjectFetcher = fetchRegistrationStatusSubject,
  relevanceStatusSubjectFetcher = fetchQualificationStatusSubject) => {
  const fetchFromAis = mockAIS ? fetchInskrivningStatusMock : inskrivningStatusFetcher;
  switch (documentType) {
    case 'JobSeekerRegistrationStatus': return registrationStatusSubjectFetcher(personnummer, fetchFromAis);
    case 'InternshipRelevanceStatus': return relevanceStatusSubjectFetcher(personnummer, fetchFromAis);
  }
  logger.warn(documentType, 'Can not handle requested document type');
  throw new Error(`Can not handle requested document type: ${documentType}`);
};

const defaultResourceLock: Set<string> = new Set();

type ContainerResourceHandlerInput = {
  resourceUri: string,
  key: any,
};

export type Effects = {
  fetchFactory: AuthFetchFactory
  audit: AuditLogger
};

const defaultEffects: Effects = {
  fetchFactory: defaultAuthFetchFactory,
  audit: defaultAuditLogger,
};

export type InboxResouorceParser = (data: string) => { outboundDataRequest: string };
export type DataRequestParser = (data: string) => {
  id: string;
  documentType: string;
  dataSubjectIdentifier: string;
  dataLocation: string;
  notificationInbox: string;
};

export type ContainerResourceHandler = (
  handleInboxResourceInput: ContainerResourceHandlerInput,
  effect?: Effects,
  inboxResouorceParser?: InboxResouorceParser,
  dataRequestLinkRemover?: DataRequestLinkRemover,
  dataRequestParser?: DataRequestParser,
  credentialSubjectFetcher?: CredentialSubjectFetcher,
  verifiableCredentialIssuer?: VerifiableCredentialIssuer,
  verifiableCredentialWriter?: VerifiableCredentialWriter,
  responseLinkWriter?: ResponseLinkWriter,
  errorWriter?: ErrorWriter) => Promise<ContainerResourceHandlerResult>;

const supportedRequestTypes = new Set([
  'JobSeekerRegistrationCertificate',
  'JobSeekerRegistrationStatus',
  'InternshipQualificationStatus',
]);

export const inboxHandlerImpl: ContainerResourceHandler = async (
  { resourceUri, key }: ContainerResourceHandlerInput,
  effects = defaultEffects,
  inboxResourceParser = parseInboxResource,
  dataRequestLinkRemover = removeDataRequestLink,
  dataRequestParser = parseDataRequestData,
  credentialSubjectFetcher = fetchCredentialSubject,
  verifiableCredentialIssuer = issueVerifiableCredential,
  verifiableCredentialWriter = writeVerifiableCredential,
  responseLinkWriter = saveLinkToInbox,
  errorWriter = writeErrorToInbox): Promise<ContainerResourceHandlerResult> => {
  const { fetchFactory, audit } = effects;
  const fetch = await fetchFactory.getAuthFetch();

  // 1. fetch DataRequestLink
  const response = await fetch(resourceUri);
  const inboxResourceData = await response.text();


  const linkData = inboxResourceParser(inboxResourceData);
  logger.trace('Parsed link resource data: %o', linkData);

  // 2. fetch OutboundDataRequest
  const response1 = await fetch(linkData.outboundDataRequest);

  if (!response1.ok) {
    if (response1.status === 403 || response1.status === 404 || response1.status === 410) {
      logger.warn(`Could not read resource at ${linkData.outboundDataRequest}, status = ${response1.status}`);
      // 7. delete DataRequestLink
      await dataRequestLinkRemover(resourceUri, fetch);
      return {
        status: 200,
        result: 'OK',
      };
    }
    return {
      status: 400, // ?????
      result: 'ERROR', // ????
    };
  }

  const outboundDataRequestData = await response1.text();

  const dataRequest = dataRequestParser(outboundDataRequestData);
  logger.debug('Extracted outboundDataRequest: %o', dataRequest);
  const requestedDocumentType = dataRequest.documentType;
  const requestedType = requestedDocumentType.slice(egendataPrefix.length);

  if (!supportedRequestTypes.has(requestedType)) {
    const error = `Document type ${requestedDocumentType} can not be handeled.`;
    return {
      status: 404,
      result: error,
    };
  }

  const personnummer = dataRequest.dataSubjectIdentifier;
  const strippedDocType = dataRequest.documentType.substring(egendataPrefix.length);
  audit.incoming(personnummer, strippedDocType);

  const re = /^\d{12}$/g;
  if (!re.test(personnummer)) {
    logger.warn(`Received data subject identifier in resource ${linkData.outboundDataRequest} does not match expected format.`);
    // 7. delete DataRequestLink
    await dataRequestLinkRemover(resourceUri, fetch);
    audit.outgoing(personnummer, 0);
    return {
      status: 200,
      result: 'OK',
    };
  }

  // 3. fetch data from AIS
  const credentialSubject = await credentialSubjectFetcher(strippedDocType, personnummer);
  logger.debug('credentialSubject: %o', credentialSubject);

  // only create verifiable credentials when user isRegistered, else put an error response in the users inbox

  if (
    requestedType !== 'JobSeekerRegistrationCertificate' ||
    requestedType === 'JobSeekerRegistrationCertificate' && credentialSubject.isRegistered) {
  // Create a Verifiable credential
    // 4. issue VC
    const verificableCredential = await verifiableCredentialIssuer(key, credentialSubject);
    logger.info('Created Verifiable Credential');
    logger.debug(verificableCredential);

    // 5. save VC in user pod
    const saveVCToDataLocationResponse = await verifiableCredentialWriter(dataRequest.id, dataRequest.dataLocation, verificableCredential, fetch);
    const response2 = saveVCToDataLocationResponse;
    logger.debug(`saveVCToDataLocationResponse[status: ${response2.status}]`);
    if (!response2.ok) {
      logger.error('Failed to save vc to data location');
      if (response2.status === 403) {
        // 7. delete DataRequestLink
        await dataRequestLinkRemover(resourceUri, fetch);
        audit.outgoing(personnummer, 0);
        return {
          status: 200,
          result: 'OK',
        };
      }
      audit.outgoing(personnummer, 0);
      return {
        status: 400, // ?????
        result: 'ERROR', // ????
      };
    }
    audit.outgoing(personnummer, 1, JSON.stringify(credentialSubject));

    // 6. save VC link in user pod
    const saveLinkResponse = await responseLinkWriter(dataRequest.notificationInbox, dataRequest.dataLocation, fetch, uuid);
    const { status } = saveLinkResponse;
    logger.debug(`saveLinkResponse[status: ${status}]`);
    if (!saveLinkResponse.ok) {
      logger.error('Failed to save link to inbox');
      if (status === 403) {
        // 7. delete DataRequestLink
        await dataRequestLinkRemover(resourceUri, fetch);
        return {
          status: 200,
          result: 'OK',
        };
      }
      return {
        status: 400, // ?????
        result: 'ERROR', // ????
      };
    }
  } else {
    const writeErrorResponse = await errorWriter(
      dataRequest.notificationInbox,
      dataRequest.id,
      linkData.outboundDataRequest,
      'E100',
      'A registration certificate is not being returned since the requested user is not enlisted at AF. Present a link for the user to register at AF.',
      fetch,
      uuid,
    );
    const { status } = writeErrorResponse;
    logger.debug(`writeErrorResponse[status: ${status}]`);
  }

  // 7. delete DataRequestLink
  await dataRequestLinkRemover(resourceUri, fetch);
  logger.info(`Successfully handled resource: ${resourceUri}`);

  audit.outgoing(personnummer, 1, JSON.stringify(credentialSubject));

  return {
    status: 200,
    result: 'OK',
  };

};

export const lockedContainerResourceHandler = async ( input: ContainerResourceHandlerInput, handler: ContainerResourceHandler, resourceLock = defaultResourceLock): Promise<ContainerResourceHandlerResult> => {
  if (resourceLock.has(input.resourceUri)) {
    const result = `Resource ${input.resourceUri} is already being handeled`;
    logger.info(result);
    return {
      result,
      status: 200,
    };
  } else {
    logger.debug(`Adding resource Lock for ${input.resourceUri}`);
    resourceLock.add(input.resourceUri);
  }
  try {
    return await handler(input, defaultEffects);
  } finally {
    resourceLock.delete(input.resourceUri);
    logger.debug(`Removed resource Lock for ${input.resourceUri}`);
  }
};

export type LockedContainerResourceHandler = (handleInboxResourceInput: ContainerResourceHandlerInput, handler?: ContainerResourceHandler) => Promise<ContainerResourceHandlerResult>;

export const inboxHandler: LockedContainerResourceHandler = async (input: ContainerResourceHandlerInput, handler: ContainerResourceHandler = inboxHandlerImpl) => {
  return lockedContainerResourceHandler(input, handler);
};

export const fetchContainerData = async (containerUrl: string, authFetch: AuthFetchFunction) => {
  const response = await authFetch(containerUrl);
  const resourceData =  await response.text();
  return resourceData;
};

const getObjectsForPredicate = (data: string, predicate: string) => {
  const N3Parser = new N3.Parser();
  const store = new N3.Store(N3Parser.parse(data));
  const objects = store.getObjects('', predicate, '');
  const resources = objects.map((o) => o.value);
  return resources;
};

export const containerResources = (containerUrl: string, data: string) => {
  const resources = getObjectsForPredicate(data, 'http://www.w3.org/ns/ldp#contains');
  return resources.map((item) => `${containerUrl}${item}`);
};

export const handleResources = async (resources: string[], key: any, handle: LockedContainerResourceHandler) => {
  for (const resourceUri of resources) {
    const result = await handle({ resourceUri, key });
    if (result.status === 500) {
      logger.fatal(`Fatal error occured: ${result.result}`);
      logger.info('Stopped handling inbox resources');
      return;
    }
  }
};

export const scanContainer = async (containerUrl: string, data: string, key: any, resourceHandler = inboxHandler) => {
  logger.debug(`Scanning container ${containerUrl}`);

  const resources = containerResources(containerUrl, data);
  await handleResources(resources, key, resourceHandler);
};

const ownerAclFragment = (resource: string, webId: string) => `<#owner>
  a acl:Authorization;
  acl:agent <${webId}>;
  acl:accessTo <${resource}>;
  acl:default <${resource}>;
  acl:mode acl:Read, acl:Write, acl:Control.

`;

const publicReadAclFragment = (resource: string) => `<#public>
  a acl:Authorization;
  acl:agentClass foaf:Agent;
  acl:accessTo <${resource}>;
  acl:default <${resource}>;
  acl:mode acl:Read.

`;

const publicWriteAclFragment = (resource: string) => `<#public>
  a acl:Authorization;
  acl:agentClass foaf:Agent;
  acl:accessTo <${resource}>;
  acl:default <${resource}>;
  acl:mode acl:Write, acl:Append.

`;

const aclTurtle = (ownerFragment: string, publicFragment: string = '', adminFragment: string = '') => `@prefix acl: <http://www.w3.org/ns/auth/acl#>.
@prefix foaf: <http://xmlns.com/foaf/0.1/>.
${ownerFragment}${publicFragment}${adminFragment}
`;

const put = async (resourceUrl: string, resourceData: string, authFetch: AuthFetchFunction, headers?: HeadersInit | undefined) => {
  const actualHeaders = headers || { 'Content-Type': 'text/turtle' };
  logger.trace(`Trying to create resource: ${resourceUrl} ...`);
  await authFetch(resourceUrl, {
    method: 'PUT',
    headers: actualHeaders,
    body: resourceData,
  });
  logger.trace(`Created: ${resourceUrl}`);
};

export type StoreFunction = (resourceUrl: string, resourceData: string, headers?: HeadersInit | undefined) => Promise<void>;

const storageFunctionFactory = (authFetch: AuthFetchFunction) => async (resourceUrl: string, resourceData: string, headers?: HeadersInit | undefined): Promise<void> => {
  await put(resourceUrl, resourceData, authFetch, headers);
};

const logo = 'data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9Im5vIj8+CjxzdmcKICAgaWQ9IkxheWVyXzEiCiAgIGRhdGEtbmFtZT0iTGF5ZXIgMSIKICAgdmlld0JveD0iMCAwIDMyIDMyIgogICB2ZXJzaW9uPSIxLjEiCiAgIHNvZGlwb2RpOmRvY25hbWU9IkFmX2xvZ290eXBfcmdiLnN2ZyIKICAgd2lkdGg9IjMyIgogICBoZWlnaHQ9IjMyIgogICBpbmtzY2FwZTp2ZXJzaW9uPSIxLjEuMiAoMGEwMGNmNTMzOSwgMjAyMi0wMi0wNCkiCiAgIHhtbG5zOmlua3NjYXBlPSJodHRwOi8vd3d3Lmlua3NjYXBlLm9yZy9uYW1lc3BhY2VzL2lua3NjYXBlIgogICB4bWxuczpzb2RpcG9kaT0iaHR0cDovL3NvZGlwb2RpLnNvdXJjZWZvcmdlLm5ldC9EVEQvc29kaXBvZGktMC5kdGQiCiAgIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIKICAgeG1sbnM6c3ZnPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIKICAgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIgogICB4bWxuczpjYz0iaHR0cDovL2NyZWF0aXZlY29tbW9ucy5vcmcvbnMjIgogICB4bWxuczpkYz0iaHR0cDovL3B1cmwub3JnL2RjL2VsZW1lbnRzLzEuMS8iPgogIDxzb2RpcG9kaTpuYW1lZHZpZXcKICAgICBpZD0ibmFtZWR2aWV3MTA5IgogICAgIHBhZ2Vjb2xvcj0iI2ZmZmZmZiIKICAgICBib3JkZXJjb2xvcj0iIzAwMDAwMCIKICAgICBib3JkZXJvcGFjaXR5PSIwLjI1IgogICAgIGlua3NjYXBlOnBhZ2VzaGFkb3c9IjIiCiAgICAgaW5rc2NhcGU6cGFnZW9wYWNpdHk9IjAuMCIKICAgICBpbmtzY2FwZTpwYWdlY2hlY2tlcmJvYXJkPSIwIgogICAgIHNob3dncmlkPSJmYWxzZSIKICAgICBpbmtzY2FwZTp6b29tPSIxNi42NjE1MzEiCiAgICAgaW5rc2NhcGU6Y3g9IjIwLjAxNjE2OCIKICAgICBpbmtzY2FwZTpjeT0iMjEuNDU2NjEyIgogICAgIGlua3NjYXBlOndpbmRvdy13aWR0aD0iMjU2MCIKICAgICBpbmtzY2FwZTp3aW5kb3ctaGVpZ2h0PSIxMzc2IgogICAgIGlua3NjYXBlOndpbmRvdy14PSIwIgogICAgIGlua3NjYXBlOndpbmRvdy15PSIyNyIKICAgICBpbmtzY2FwZTp3aW5kb3ctbWF4aW1pemVkPSIxIgogICAgIGlua3NjYXBlOmN1cnJlbnQtbGF5ZXI9IkxheWVyXzEiIC8+CiAgPGRlZnMKICAgICBpZD0iZGVmczQiPgogICAgPHN0eWxlCiAgICAgICBpZD0ic3R5bGUyIj4uY2xzLTF7ZmlsbDojMDAwMDVhO30uY2xzLTJ7ZmlsbDojN2VjMTNkO308L3N0eWxlPgogIDwvZGVmcz4KICA8dGl0bGUKICAgICBpZD0idGl0bGU2Ij5BcnRib2FyZCAxPC90aXRsZT4KICA8cmVjdAogICAgIHN0eWxlPSJmaWxsOiMwMDAwNWE7ZmlsbC1vcGFjaXR5OjE7c3Ryb2tlLXdpZHRoOjAuMjM1Nzk1IgogICAgIGlkPSJyZWN0NzM1NiIKICAgICB3aWR0aD0iMzIiCiAgICAgaGVpZ2h0PSIzMiIKICAgICB4PSIwIgogICAgIHk9IjAiIC8+CiAgPG1ldGFkYXRhCiAgICAgaWQ9Im1ldGFkYXRhNzQzOCI+CiAgICA8cmRmOlJERj4KICAgICAgPGNjOldvcmsKICAgICAgICAgcmRmOmFib3V0PSIiPgogICAgICAgIDxkYzp0aXRsZT5BcnRib2FyZCAxPC9kYzp0aXRsZT4KICAgICAgPC9jYzpXb3JrPgogICAgPC9yZGY6UkRGPgogIDwvbWV0YWRhdGE+CiAgPGcKICAgICBpZD0iZzc0NDIiCiAgICAgdHJhbnNmb3JtPSJtYXRyaXgoMC4zMzE5MTM2NCwwLDAsMC4zMjk4ODc4NCwxLjAwMzMxOTEsMS4wMDAwMDAxKSI+CiAgICA8cGF0aAogICAgICAgY2xhc3M9ImNscy0yIgogICAgICAgZD0iTSAzMC44MiwyOC43MiBBIDMwLjgzLDMwLjgzIDAgMSAwIDYxLjY1LDU5LjU1IDMwLjgzLDMwLjgzIDAgMCAwIDMwLjgyLDI4LjcyIFogbSAwLDQ3LjMgQSAxNi40NywxNi40NyAwIDEgMSA0Ny4yOSw1OS41NSAxNi40NywxNi40NyAwIDAgMSAzMC44Miw3NiBaIgogICAgICAgaWQ9InBhdGgxMDQiIC8+CiAgICA8cGF0aAogICAgICAgY2xhc3M9ImNscy0yIgogICAgICAgZD0iTSAzMC44MiwwIFYgMTQuNDEgQSA0NS4xNSw0NS4xNSAwIDAgMSA2OS40Myw4MyBsIDEyLDcuOTQgQSA1OS41NSw1OS41NSAwIDAgMCAzMC44MiwwIFoiCiAgICAgICBpZD0icGF0aDEwNiIgLz4KICA8L2c+Cjwvc3ZnPgo=';

const initializeEgendata = async (webId: string, storagePodUrl: string, store: StoreFunction) => {
  const storagePodInboxUrl = `${storagePodUrl}egendata/inbox/`;
  const storagePodInboxAclUrl = `${storagePodUrl}egendata/inbox/.acl`;
  logger.debug(`Try to create egendata/inbox acl: ${storagePodInboxAclUrl}`);
  const storagePodInboxAclTurtle = aclTurtle(ownerAclFragment(storagePodInboxUrl, webId), publicWriteAclFragment(storagePodInboxUrl));
  await store(storagePodInboxAclUrl, storagePodInboxAclTurtle);

  // https://developer.mozilla.org/en-US/docs/Web/HTTP/Basics_of_HTTP/Data_URLs
  const profileUrl = webId.substring(0, webId.lastIndexOf('/'));
  const logoUrl = `${profileUrl}/logo`;
  logger.debug(`Try to create logo: ${logoUrl}`);
  await store(logoUrl, logo, { 'Content-Type': 'text/plain' });

  const logoAclUrl = `${logoUrl}.acl`;
  logger.debug(`Try to create logo acl: ${logoAclUrl}`);
  await store(logoAclUrl, aclTurtle(ownerAclFragment(logoAclUrl, webId), publicReadAclFragment(logoAclUrl)));
};

export const egendataInitialization = async (webId: string, storagePodUrl: string, authFetch: AuthFetchFunction) => {
  const inboxUrl = `${storagePodUrl}egendata/`;
  const response = await authFetch(inboxUrl, { method: 'HEAD' });  
  if (response.status === 404) {
    await initializeEgendata(webId, storagePodUrl, storageFunctionFactory(authFetch));
  }
};

export const scanInboxWithNewToken = async (inboxUrl: string, key: any, authFetchFactory: AuthFetchFactory, containerDataFetcher = fetchContainerData, containerScanner = scanContainer) => {
  const authFetch = await authFetchFactory.getAuthFetch();
  const data =  await containerDataFetcher(inboxUrl, authFetch);
  await containerScanner(inboxUrl, data, key);
};

// test exports
// eslint-disable-next-line @typescript-eslint/naming-convention
export const __getSubscriptionUrl = getSubscriptionUrl;
// eslint-disable-next-line @typescript-eslint/naming-convention
export const __getBaseUrl = getBaseUrl;
// eslint-disable-next-line @typescript-eslint/naming-convention
export const __removeDataRequestLink = removeDataRequestLink;
// eslint-disable-next-line @typescript-eslint/naming-convention
export const __fetchCredentialSubject = fetchCredentialSubject;
// eslint-disable-next-line @typescript-eslint/naming-convention
export const __getObjectsForPredicate = getObjectsForPredicate;
// eslint-disable-next-line @typescript-eslint/naming-convention
export const __put = put;
// eslint-disable-next-line @typescript-eslint/naming-convention
export const __initializeEgendata = initializeEgendata;
