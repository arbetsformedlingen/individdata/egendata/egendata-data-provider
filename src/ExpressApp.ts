import errorHandler from 'errorhandler';
import express, { Application } from 'express';
import compression from 'compression';
import { subscribeToInbox, storageUrlFromProfile, fetchContainerData, scanContainer, scanInboxWithNewToken, egendataInitialization } from './util/solid';
import { WebhookController } from './controller/webhookController';
import { keyPath, webid, port, scanInboxIntervalMinutes } from './config';
import { loadKey } from './util/vc';
import { logger } from './util/logger';
//import { defaultAuthFetchFactory } from './util/OidcAuthFetchFactory';
import { AuthFetchFunction } from './util/AuthFetchFactory';
import { defaultAuthFetchFactory } from './util/OidcAuthFetchFactory';

const environment = ['development', 'test', 'production'].includes(process.env.NODE_ENV!) ? process.env.NODE_ENV : 'production';

export class ExpressApp {
  private app?: Application;

  public getApp(): Application {
    if (!this.app) {
      const app = express();
      try {
        //app.set('env', environment);
        app.set('port', port);
        app.use(express.json({ type: ['application/json', 'application/ld+json'] }));
        app.use(express.urlencoded({ extended: true }));
        app.use(compression());
        
        if (environment === 'development') {
          app.use(errorHandler());
        }
        
        app.get('/', (req: any, res: any) => res.send('Welcome to Egendata Data Provider'));
      } catch (error) {
        logger.error(error);
        throw error;
      }
      this.app = app;
    }
    return this.app;
  }

  public async initialize(authFetch: AuthFetchFunction) {
    const response =  await fetch(webid);
    const profile = await response.text();
    const storagePodUrl = storageUrlFromProfile(profile);
  
    logger.debug('Loading key from: %o', keyPath);
    const key = await loadKey(keyPath, { id: `${storagePodUrl}key`, controller: `${storagePodUrl}controller` });
    const webHookController = new WebhookController(key);
  
    this.getApp().post('/webhook', (req, res) => {
      webHookController.handle(req, res).catch((err) => {
        logger.error('Failed to handle webhook request, error: %o', err);
        res.status(500).end();
      });
    });
    
    try {
      // check/create egendata resources
      await egendataInitialization(webid, storagePodUrl, authFetch);
  
      const inboxUrl = `${storagePodUrl}egendata/inbox/`;
      const data =  await fetchContainerData(inboxUrl, authFetch);
      logger.info('Scanning inbox');
      await scanContainer(inboxUrl, data, key);
      //    await scanInbox(storagePodUrl, key, authFetch);
      logger.info(`Initializng timer for scanning inbox every ${scanInboxIntervalMinutes} minute`);
      setInterval(scanInboxWithNewToken, parseInt(scanInboxIntervalMinutes) * 60 * 1000, inboxUrl, key, defaultAuthFetchFactory);
  
      logger.info('Subscribing to inbox notifications');
      const unsubscribeEndpoint = await subscribeToInbox(storagePodUrl, authFetch);
      logger.info('Subscription done');
      return { storagePodUrl, unsubscribeEndpoint };
    } catch (err) {
      logger.error('Application error: %o', err);
      throw err;
    }
  }
}
