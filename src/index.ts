import { ExpressApp } from './ExpressApp';
import { Server } from './server';
import { logger } from './util/logger';

const expressApp = new ExpressApp();

const server = new Server(expressApp);

process.once('SIGTERM', async () => {
  logger.info('SIGTERM signal received: closing HTTP server');
  await server.terminate();
});

process.once('SIGINT', async () => {
  logger.info('SIGINT signal received: closing HTTP server');
  await server.terminate();
});

server.getHttpServer().then((httpServer) => {
  httpServer.listen(expressApp.getApp().get('port'), () => {
    logger.info(`App is running at http://localhost:${expressApp.getApp().get('port')} in ${expressApp.getApp().get('env')} mode`);
  });
});
