import { LoggerOptions } from 'pino';
import { logLevel, nodeEnv } from './config';

export const options: LoggerOptions = {
  enabled: !process.env.TESTING,
  level: logLevel || 'trace',
  base: {
    env: nodeEnv,
  },
};
