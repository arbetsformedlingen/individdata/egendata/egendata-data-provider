import { Logger } from '../Logger';
import { AuditLogger, auditRecord, ExecInfo } from './AuditLogger';

export class AuditLoggerImpl implements AuditLogger {
  private logger;

  constructor(logger: Logger) {
    this.logger = logger;
  }

  incoming = (pnr: string, doctype: string) => {
    const record = auditRecord(pnr, 1, [`eventType=Begäran om egen hämtning, docType=dataRequest, requestType=${doctype}`]);
    this.logger.log(JSON.stringify(record));
  };
  
  outgoing = (pnr: string, executioninfo: ExecInfo, credentialSubject?: string) => {
    if (executioninfo === 0 && credentialSubject) {
      throw new Error('Unexpected credentialSubject when executionInfo is set to zero in the outgoing audit log attempt.');
    }
    const objectInfo = credentialSubject ? [`eventType=Utlämnande av egen hämtning, docType=verifiableCredential, credentialSubject=${credentialSubject}`] : undefined;
    const record = auditRecord(pnr, executioninfo, objectInfo);
    this.logger.log(JSON.stringify(record)); 
  };
}
