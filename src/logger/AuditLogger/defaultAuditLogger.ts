import { ConsoleLogger } from '../ConsoleLogger';
import { AuditLoggerImpl } from './AuditLoggerImpl';

export const defaultAuditLogger = new AuditLoggerImpl(new ConsoleLogger());
