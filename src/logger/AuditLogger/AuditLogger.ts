type User = {
  name: string,
};

type LogEvent = {
  action: string,
};

type Application = {
  name: string,
  version?: string,
};

export type ExecInfo = 0 | 1;

export type Audit = {
  objecttype: string
  objectidtype: string,
  objectid: string,
  objectinfo?: string[],
  executioninfo: ExecInfo,
};

export const audit = (pnr: string, executioninfo: ExecInfo, objectinfo?: string[]): Audit => {
  return (objectinfo) ?
    {
      objecttype: 'Person',
      objectidtype: 'Personnummer',
      objectid: pnr,
      objectinfo,
      executioninfo,
    } :
    {
      objecttype: 'Person',
      objectidtype: 'Personnummer',
      objectid: pnr,
      executioninfo,
    };
};

export type AuditRecord = {
  '@timestamp' : string,
  user: User,
  event: LogEvent,
  application: Application,
  audit : Audit,
};

export const auditRecord = (pnr: string, executioninfo: ExecInfo, objectInfo?: string[]): AuditRecord => {
  const record = {
    '@timestamp' : new Date().toISOString(),
    user: {
      name: '',
    },
    event: {
      action: 'Read',
    },
    application: {
      name : 'egendata-data-provider',
    },
    audit : audit(pnr, executioninfo, objectInfo),
  };
  return record;
};

export interface AuditLogger {
  incoming: (pnr: string, doctype: string) => void;
  outgoing: (pnr: string, executioninfo: ExecInfo, credentialSubject?: string) => void;
}
