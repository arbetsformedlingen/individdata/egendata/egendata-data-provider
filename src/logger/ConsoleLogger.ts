import { Logger } from './Logger';

export class ConsoleLogger implements Logger {
  log(record: string) {
    console.log(record);
  }
}
