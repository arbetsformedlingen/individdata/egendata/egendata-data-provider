export interface Logger {
  log: (record: string) => void;
}
