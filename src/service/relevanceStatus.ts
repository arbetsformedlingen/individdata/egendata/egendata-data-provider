import type { FetchInskrivningStatusFn } from './ais/inskrivning';

export type QualificationStatus = {
  isRegistered: boolean,
  registrationDate: string,
  isRelevant: boolean,
};

export type QualificationStatusSubject = {
  type: 'InternshipQualificationStatus',
  subject: string,
  isRegistered: boolean,
  registrationDate: string,
  isRelevant: boolean,
};

export type QualificationStatusSubjectFetcher = (personummer: string, fetchFn: FetchInskrivningStatusFn) => Promise<QualificationStatusSubject>;

async function fetchQualificationStatus(personummer: string, fetchFn: FetchInskrivningStatusFn): Promise<QualificationStatus> {
  const inskrivning = await fetchFn(personummer);
  const isRegistered = (inskrivning.arbetssokande_status === 'Aktuell' || inskrivning.arbetssokande_status === 'Registrerad');
  const registrationDate = inskrivning.inskrivningsdatum;
  const isRelevant = (inskrivning.sokandekategori_kod === '11' ||
    inskrivning.sokandekategori_kod === '68' ||
    inskrivning.sokandekategori_kod === '69' ||
    inskrivning.sokandekategori_kod === '70' ||
    inskrivning.sokandekategori_kod === '22');
  return {
    isRegistered,
    registrationDate,
    isRelevant,
  };
}



export async function fetchQualificationStatusSubject(personummer: string, fetchFn: FetchInskrivningStatusFn): Promise<QualificationStatusSubject> {
  const { isRegistered, registrationDate, isRelevant } = await fetchQualificationStatus(personummer, fetchFn);
  return {
    type: 'InternshipQualificationStatus',
    subject: personummer,
    isRegistered,
    registrationDate,
    isRelevant,
  };
}