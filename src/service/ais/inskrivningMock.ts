import { randomInt } from 'crypto';

interface InskrivningStatus {
  'arbetssokande_status': string,
  'inskrivningsdatum': string,
  'sokandekategori_kod': string
}

export type FetchInskrivningStatusFn = (personnummer: string) => Promise<InskrivningStatus>;

// eslint-disable-next-line @typescript-eslint/no-unused-vars
export async function fetchInskrivningStatus(test: string): Promise<InskrivningStatus> {
  if (test === 'test') {
    return {
      'arbetssokande_status': 'test',
      'inskrivningsdatum': 'test',
      'sokandekategori_kod': 'test',
    };
  }
  if (randomInt(0, 2) === 1) {
    return {
      'arbetssokande_status': 'Aktuell',
      'inskrivningsdatum': '2023-01-04',
      'sokandekategori_kod': '11',
    };
  } else {
    return {
      'arbetssokande_status': 'Avskriven',
      'inskrivningsdatum': '',
      'sokandekategori_kod': '23',
    };
  }
}