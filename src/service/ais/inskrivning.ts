import https from 'https';
import { Guid } from 'guid-typescript';
import { aisBaseUrl, aisClientId, aisClientSecret, aisEnvironment, aisSystemId, aisProxyUrl } from '../../config';
import HttpsProxyAgent from 'https-proxy-agent';
import { logger } from '../../util/logger';

enum AISEnvironment {
  U1 = 'U1',
  I1 = 'I1',
  T1 = 'T1',
  T2 = 'T2',
  PROD = 'PROD',
}
export interface InskrivningStatus {
  'arbetssokande_status': string,
  'inskrivningsdatum': string,
  'sokandekategori_kod': string
}

export type FetchInskrivningStatusFn = (personnummer: string, fetcher?: typeof fetch) => Promise<InskrivningStatus>;

export async function fetchInskrivningStatus(personummer: string, fetcher = fetch): Promise<InskrivningStatus> {
  const httpsAgent = new https.Agent({  
    rejectUnauthorized: false,
  });
  const config = {
    headers: {
      'AF-TrackingId': Guid.create().toString(),
      'AF-SystemId': aisSystemId,
      'AF-Environment': AISEnvironment[aisEnvironment as AISEnvironment],
    },
    params: {
      client_id: aisClientId,
      client_secret: aisClientSecret,
    },
    agent: aisProxyUrl ? HttpsProxyAgent(aisProxyUrl) : httpsAgent,
  };
  const url = `${aisBaseUrl}/${personummer}`;
  const response = await fetcher(url, config);
  if (!response.ok) {
    logger.error('error message: %o', response.statusText);
    throw new Error(response.statusText);
  }

  // TODO: parse with zod !
  return response.json() as Promise<InskrivningStatus>;
}