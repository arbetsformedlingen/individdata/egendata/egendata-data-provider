FROM node:16.20.2-alpine3.18 AS builder
WORKDIR /app

COPY package.json package-lock.json ./
RUN npm install -g npm@9.8.1 &&\
    npm clean-install
COPY . .
RUN cat test-services-jtech-se.pem >> /etc/ssl/certs/ca-certificates.crt &&\
    npm run build

FROM node:16.20.2-alpine3.18 AS server
RUN apk add --no-cache openssl
WORKDIR /app

COPY package.json package-lock.json ./
COPY --from=builder --chown=node:node /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/ca-certificates.crt
RUN npm install -g npm@9.8.1 &&\
    npm install --omit=dev
COPY --from=builder --chown=node:node /app/dist .

RUN mkdir -p /key

COPY ./entrypoint.sh /
ENTRYPOINT [ "/entrypoint.sh" ]
CMD ["node", "index"]

EXPOSE 3002
