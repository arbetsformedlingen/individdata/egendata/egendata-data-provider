jest.mock('pino', () => {
  return {
    transport: jest.fn(), pino: jest.fn(() => {
      return {
        fatal: jest.fn(),
        error: jest.fn(),
        warn: jest.fn(),
        info: jest.fn(),
        debug: jest.fn(),
        trace: jest.fn(),
        }
    })
  };
});
