
# Egendata Data Provider
An example data provider for the Solid based Egendata Infrastructure written in Typescript.

The data provider is developed for a special pilot case providing information from the Arbetsförmedlingen AIS inskrivning API accessed through the Arbetsförmedlingen integration platform.

While starting up, the data provider is being authenticated as the configured service user at the Egendata OpenID Provider, starts a timer to scan the service user's Egendata inbox periodically, and subscribes to notifications for resources being added to that inbox. (In reality only one of the methods would be neeeded)

The Egendata Wallet application makes data requests for the user by creating a data request resouce in the users pod and gives the data provider read access to it. Then it puts a data request link resource in the data providers egendata inbox.
[See sequence diagram.](https://gitlab.com/groups/arbetsformedlingen/individdata/egendata/-/wikis/home#sequence-diagram)

Whenever a data request is being detected by the data provider, (either by scanning the inbox or getting a notificaction), it follows the request link, reads the data request, and if the data type being specified in the request matches the one provided by the data provider the requested information is being fetched from the configured service.

The information fetched is being wrapped in a Verifiable Credential and is put into the resource URL specified by the data request, (where the data provider has been given write access).

Finally there is a data response link being written to the users egendata inbox pointing the written verifiable credential resource.

Some of the most expected error cases has been taken care of by implementing error handling.

The data provider is created as a service and handles SIGINT events gracefully by unsubscribing from previous subscriptions.

## To Build and run you need to have:
1. Node.js >= 14.0
2. Build tool either npm or yarn (prefreable npm) 

## Install All Dependencies
```
npm install
```

## Build
```
npm run build
```
## Start the Server
```
npm start
```

## Test
```
npm run test
```



