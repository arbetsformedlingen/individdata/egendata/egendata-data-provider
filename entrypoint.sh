#!/bin/sh
set -e

KEYFILE="${KEY_PATH:=/key/source-key.pem}"

if [ ! -f "$KEYFILE" ]; then
  openssl genpkey -algorithm ed25519 -out $KEYFILE
fi

chown -R node:node /key && chmod 700 /key

exec "$@"
